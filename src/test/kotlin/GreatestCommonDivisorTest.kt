package tech.materialize.math.test

import tech.materialize.math.greatestCommonDivisor
import kotlin.test.Test
import kotlin.test.assertEquals

class GreatestCommonDivisorTest {

    @Test
    fun `both numbers is zero`() {
        assertEquals(0, greatestCommonDivisor(0, 0))
    }

    @Test
    fun `smaller number is zero`() {
        assertEquals(1, greatestCommonDivisor(1, 0))
    }

    @Test
    fun `both numbers are one`() {
        assertEquals(1, greatestCommonDivisor(1, 1))
    }

    @Test
    fun `smaller number is one`() {
        assertEquals(1, greatestCommonDivisor(4, 1))
    }

    @Test
    fun `two and two`() {
        assertEquals(2, greatestCommonDivisor(2, 2))
    }

    @Test
    fun `negative number`() {
        assertEquals(1, greatestCommonDivisor(-1, -1))
    }

    @Test
    fun `prime relative to each other`() {
        assertEquals(1, greatestCommonDivisor(2, 3))
        assertEquals(1, greatestCommonDivisor(4, 7))
        assertEquals(1, greatestCommonDivisor(-2, -3))
    }

    @Test
    fun `multiples of each other`() {
        assertEquals(2, greatestCommonDivisor(2, 4))
        assertEquals(3, greatestCommonDivisor(3, 12))
        assertEquals(5, greatestCommonDivisor(5, 10))
    }

    @Test
    fun `one cycle`() {
        // 8 and 6 gcd can be found with one step of Euclid's Algorithm
        assertEquals(2, greatestCommonDivisor(8, 6))
    }

    @Test
    fun `multiple cycles`() {
        // 33 and 27 gcd can be found with more than one step of Euclid's Algorithm
        assertEquals(3, greatestCommonDivisor(33, 27))
    }

    @Test
    fun `multiple cycles and both negative`() {
        assertEquals(12, greatestCommonDivisor(-24, -36))
    }

    @Test
    fun `multiple cycles and greater absolute value is negative `() {
        assertEquals(12, greatestCommonDivisor(24, -36))
    }

    @Test
    fun `multiple cycles and smaller absolute value is negative`() {
        assertEquals(12, greatestCommonDivisor(36, -24))
    }

}
