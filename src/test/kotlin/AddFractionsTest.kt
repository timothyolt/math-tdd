package tech.materialize.math.test

import tech.materialize.math.Fraction
import kotlin.test.*
import kotlin.test.assertEquals

class AddFractionsTest  {
    @Test
    fun `zero plus zero`() {
        val sum = Fraction(0) + Fraction(0)
        assertEquals(Fraction(0), sum)
    }

    @Test
    fun `zero plus nonzero`() {
        val sum = Fraction(0) + Fraction(1)
        assertEquals(Fraction(1), sum)
    }

    @Test
    fun `nonzero plus zero`() {
        val sum = Fraction(2) + Fraction(0)
        assertEquals(Fraction(2), sum)
    }

    @Test
    fun `nonzero plus nonzero`() {
        val sum = Fraction(1) + Fraction(2)
        assertEquals(Fraction(3), sum)
    }

    @Test
    fun `negative inputs negative output`() {
        val sum = Fraction(-1) + Fraction(-2)
        assertEquals(Fraction(-3), sum)
    }

    @Test
    fun `non-trivial but same denominator`() {
        val sum = Fraction(1, 5) + Fraction(2, 5)
        assertEquals(Fraction(3, 5), sum)
    }

    @Test
    fun `different denominators but cannot be reduced`() {
        val sum = Fraction(1, 3) + Fraction(1, 2)
        assertEquals(Fraction(5, 6), sum)
    }

    @Test
    fun `different denominators that are not multiples and can be reduced`() {
        val sum = Fraction(2, 4) + Fraction(1, 3)
        assertEquals(Fraction(10, 12), sum)
    }

    @Test
    fun `different denominators that are multiples and can be reduced`() {
        // I think this replicates some idiosyncrasies of doing fractions by hand
        val sum = Fraction(1, 3) + Fraction(1, 6)
        assertEquals(Fraction(3, 6), sum)
    }

    @Test
    fun `different denominators that are multiples and cannot be reduced`() {
        // I think this replicates some idiosyncrasies of doing fractions by hand
        val sum = Fraction(1, 8) + Fraction(1, 16)
        assertEquals(Fraction(3, 16), sum)
    }
}
