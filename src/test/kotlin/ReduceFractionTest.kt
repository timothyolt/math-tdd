package tech.materialize.math.test

import tech.materialize.math.Fraction
import kotlin.test.*

class ReduceFractionTest {
    @Test
    fun `already reduced stays the same`() {
        assertEquals(Fraction(3, 2), Fraction(3, 2).reduced())
    }
    
    @Test
    fun `reduce to not whole number with trivial case`() {
        assertEquals(Fraction(3, 4), Fraction(6, 8).reduced())
    }

    @Test
    fun `reduce to not whole number with non-trivial case`() {
        assertEquals(Fraction(9, 11), Fraction(27, 33).reduced())
    }

    @Test
    fun `numerator and denominator negative`() {
        assertEquals(Fraction(1, 1), Fraction(-1, -1).reduced())
    }

    @Test
    fun `denominator negative`() {
        assertEquals(Fraction(-1, 1), Fraction(1, -1).reduced())
    }

    @Test
    fun `numerator negative`() {
        assertEquals(Fraction(-1, 1), Fraction(-1, 1).reduced())
    }

    @Test
    fun `zero divided by zero`() {
        assertEquals(Fraction(0, 0), Fraction(0, 0).reduced())
    }

}
