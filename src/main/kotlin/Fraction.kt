package tech.materialize.math

import kotlin.math.max
import kotlin.math.min

data class Fraction(val numerator: Int, val denominator: Int = 1) {

    operator fun plus(other: Fraction): Fraction {
        return if (denominator == other.denominator) {
            Fraction(numerator + other.numerator, other.denominator)
        } else {
            val crossProductAddition = crossProductAddition(other)
            reduceToLargerMultiple(other, crossProductAddition)
        }
    }

    private fun crossProductAddition(other: Fraction) = Fraction(
        numerator = numerator * other.denominator + other.numerator * denominator,
        denominator = denominator * other.denominator
    )

    private fun reduceToLargerMultiple(
        other: Fraction,
        crossProductAddition: Fraction
    ): Fraction {
        val biggerDenominator = max(denominator, other.denominator)
        val smallerDenominator = min(denominator, other.denominator)
        return if (biggerDenominator % smallerDenominator == 0) {
            Fraction(
                crossProductAddition.numerator / smallerDenominator,
                crossProductAddition.denominator / smallerDenominator
            )
        } else {
            crossProductAddition
        }
    }

    fun reduced(): Fraction {
        if (denominator == 0) return this
        val gcd = greatestCommonDivisor(numerator, denominator)
        val reduced = Fraction(numerator / gcd, denominator / gcd)
        return reduced.normalize()
    }

    private fun normalize() = if (denominator < 0) {
        Fraction(numerator * -1, denominator * -1)
    } else {
        this
    }

}
