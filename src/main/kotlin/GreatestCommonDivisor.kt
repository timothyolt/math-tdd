package tech.materialize.math

import kotlin.math.abs

fun greatestCommonDivisor(number: Int, anotherNumber: Int): Int =
    abs(greatestCommonDivisorMeat(number, anotherNumber))

// Euclid's Algorithm
private tailrec fun greatestCommonDivisorMeat(number: Int, anotherNumber: Int): Int =
    if (anotherNumber == 0) {
        number
    } else {
        greatestCommonDivisorMeat(anotherNumber, number.mod(anotherNumber))
    }
